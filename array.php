<?php

//$buah[2] = "Anggur";
//$buah[3] = "Jeruk";
//$buah[5] = "Pepaya";
//echo $buah[3];


// membuat array asosiatif
//$artikel = [
 //   "judul" => "Pemrograman web array",
   // "penulis" => "Maulana Husaini",
    //"view" => 128
//];

// mencetak isi array assosiatif
//echo "<h2>".$artikel["judul"]."</h2>";
//echo "<p>oleh: ".$artikel["penulis"]."</p>";
//echo "<p>View: ".$artikel["view"]."</p>";


// membuat array
//$barang = ["Buku Tulis", "Penghapus", "Spidol"];

// menampilkan isi array dengan perulangan foreach
//foreach($barang as $isi){
   // echo $isi."<br>";
//}

//echo "<hr>";

// menampilkan isi array dengan perulangan while
//$i = 0;
//while($i < count($barang)){
  //  echo $barang[$i]."<br>";
    //$i++;
//}
//$angkas = array(1,2,3,4,5,6,7,8,9,10);
//print_r($angkas); //Versi singkat
//echo "<br /><br />";
//echo "<pre>".print_r($angkas, true)."</pre>";


// Define array
//$colors = array("Red", "Green", "Blue", "Yellow");
  
// Sorting and printing array
//sort($colors);
//print_r($colors);


// Define array
//$colors = array("Red", "Green", "Blue", "Yellow");
  
// Sorting and printing array
//rsort($colors);
//print_r($colors);

// Define array
//$age = array("Peter"=>20, "Harry"=>14, "John"=>45, "Clark"=>35);
  
// Sorting array by key and print
//ksort($age);
//print_r($age);
// Define array
//$age = array("Peter"=>20, "Harry"=>14, "John"=>45, "Clark"=>35);
  
// Sorting array by key and print
//krsort($age);
//print_r($age);

/*inisialisasi variabel mahasiswa dengan nilai berformat array*/
$mahasiswa = array( "Adi", "Tono", "Dila", "Maun" );

//menampilkan keluaran hasil Array ( [0] => Adi [1] => Tono [2] => Dila [3] => Maun )
print_r($mahasiswa);

//menampilkan keluaran "saat ini pointer dalam array menunjuk pada key atau indeks 0"
echo "saat ini pointer dalam array menunjuk pada key atau indeks ".key($mahasiswa);



?>
